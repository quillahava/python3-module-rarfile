%define _unpackaged_files_terminate_build 1
%define pypi_name rarfile

%def_with check

Name: python3-module-%pypi_name
Version: 4.2
Release: alt1
Summary: RAR archive reader for Python
License: ISC
Group: Development/Python3
Url: https://pypi.org/project/rarfile/
Vcs: https://github.com/markokr/rarfile
BuildArch: noarch
Source0: %name-%version.tar

BuildRequires(pre): rpm-build-python3
BuildRequires: python3(setuptools)
BuildRequires: python3(wheel)

%if_with check
BuildRequires: python3(pytest)
BuildRequires: unrar
BuildRequires: p7zip
BuildRequires: bsdtar
%endif

%description
This is a Python module for RAR archive reading. The interface is made as
zipfile-like as possible.

%prep
%setup

%build
%pyproject_build

%install
%pyproject_install

%check
%pyproject_run_pytest -ra

%files
%doc README.rst LICENSE MANIFEST.in
%python3_sitelibdir/%pypi_name.py
%python3_sitelibdir/__pycache__/
%python3_sitelibdir/%{pyproject_distinfo %pypi_name}

%changelog
* Tue Jun 25 2024 Aleksandr A. Voyt <sobue@altlinux.org> 4.2-alt1
- Initial commit
